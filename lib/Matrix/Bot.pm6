#! /usr/bin/env false

use v6.d;

use LogP6;
use Matrix::Client;
use Text::Markdown;

use Matrix::Bot::EventWrapper::InitialSync;
use Matrix::Bot::EventWrapper::RoomInvite;
use Matrix::Bot::EventWrapper::RoomTextMessage;
use Matrix::Bot::Plugin;

#| A small framework to build Matrix bots upon.
unit class Matrix::Bot;

#| The URL of the homeserver you want to connect with.
has Str $.home-server = "https://matrix.org";

#| The username of the account used by the bot.
has Str $.username is required;

#| The password of the account used by the bot. If you already have an access
#| token, you can leave the password empty.
has Str $.password;

#| The access token to authorize to the API with. If you don't have one yet,
#| use a password.
has Str $.access-token;

#| The device ID reporte by the bot. This defaults to Matrix::Bot, but you can
#| change this to any string you like.
has Str $.device-id = "Matrix::Bot";

#| The timer to sync for new events. This defaults to 5, but can be altered as
#| needed.
has Int $.sync-timer is rw = 5;

#| A list of Matrix::Bot::Plugin objects. Events will be distributed to all
#| plugins in this list.
has Matrix::Bot::Plugin @.plugins = [];

#| The Matrix::Client object that is used internally to deal with the Matrix
#| API.
has Matrix::Client $.client;

method TWEAK
{
	# Ensure credentials are supplied
	if (!$!access-token && !$!password) {
		die "A password or access-token is required";
	}

	# Create the Matrix client
	$!client .= new(:$!home-server, :$!device-id, :$!access-token);
}

#| Run the bot's event-loop. This will start gathering events from Matrix, and
#| hand them off to registered plugins.
method run ()
{
	get-logger("Matrix::Bot").debug("Entering event loop");

	my $since = "";
	my $event-handler = Supplier.new;

	# Authenticate the client
	if (!$!access-token) {
		$!client.login(:$!username, :$!password);
		$!access-token = $!client.access-token;
	}

	get-logger("Matrix::Bot").info("Connected as {$!client.whoami}");

	# Retrieve initial timestamp, without history
	{
		my $data = $!client.sync(
			sync-filter => { room => timeline => limit => 0 }
		);

		$since = $data.next-batch;

		my @rooms = $data.joined-rooms.map(*.room-id);

		# Distribute handle-connect event
		self!distribute("handle-connect", sub ($plugin) {
			my $event = Matrix::Bot::EventWrapper::InitialSync.new(
				bot => self,
				:@rooms,
			);

			$plugin.handle-connect($event)
		});
	}

	# Get all the plugin handlers
	my @plugin-handlers = @!plugins».event-handler;

	react {
		whenever Supply.merge(@plugin-handlers) -> $plugin-event {
			self.send-markdown($plugin-event.room-id, $plugin-event.message);
		}
		# Fetch new events
		whenever Supply.interval($!sync-timer).skip {
			my $data = $!client.sync(:$since);

			# Set since limit, to avoid getting duplicates
			$since = $data.next-batch;

			# Go through all rooms we're in
			for $data.joined-rooms -> $room {
				# Emit all events to the event-handler
				for $room.timeline.events -> $event {
					$event-handler.emit($event);
				}
			}

			# Handle invites that have been sent
			for $data.invited-rooms -> $invite {
				$event-handler.emit($invite);
			}
		}

		# Distribute the events to plugins
		whenever $event-handler.Supply -> $event {
			self!distribute("handle-event", sub ($plugin) {
				$plugin.handle-event($event)
			})
		}

		#
		# Convenience methods for end-users
		#

		# handle-invite
		whenever $event-handler
			.Supply
			.grep(* ~~ Matrix::Response::InviteInfo)
			-> $event
		{
			self!distribute("handle-invite", sub ($plugin) {
				my %data =
					bot => self,
					room => $event.room-id,
					:$event,
				;
				my Str @aliases;

				for $event.events -> $meta {
					given $meta.type.fc {
						when "m.room.canonical_alias" {
							@aliases.append: $meta.content<alias>;
						}
						when "m.room.name" {
							%data<name> = $meta.content<name>;
						}
						when "m.room.member" {
							# The m.room.member type seemingly doesn't always
							# come with a timestamp.
							if ($meta.origin_server_ts.defined) {
								%data<time> = DateTime.new($meta.origin_server_ts ÷ 1000);
							}

							%data<user> = $meta.sender;
						}
					}
				}

				my $wrapper = Matrix::Bot::EventWrapper::RoomInvite.new(|%data, :@aliases);

				my $response = $plugin.handle-invite($wrapper);

				if ($response) {
					$!client.join-room($event.room-id);
				}
			})
		}

		# handle-room-text
		whenever $event-handler
			.Supply
			.grep(* ~~ Matrix::Response::StateEvent)
			.grep(*.type eq "m.room.message")
			.grep(*.content<msgtype> eq "m.text")
			.grep(*.sender ne $!client.whoami)
			-> $event
		{
			self!distribute("handle-room-text", sub ($plugin) {
				my $wrapper = Matrix::Bot::EventWrapper::RoomTextMessage.new(
					bot => self,
					message => $event.content<body>,
					room => $event.room_id,
					time => DateTime.new($event.origin_server_ts ÷ 1000),
					user => $event.sender,
					:$event,
				);

				my $reply = $plugin.handle-room-text($wrapper);

				# If the handler returns a string, conveniently send it back to the
				# originating channel.
				if ($reply ~~ Str:D && $reply ne "") {
					self.send-markdown($event.room_id, $reply);
				}
			})
		}
	}
}

#| Convenience method to send rendered Markdown to a room.
method send-markdown (
	#| The room ID to send the message to.
	Matrix::RoomID:D $room-id,

	#| The Markdown body to send.
	Str:D $body,

	#| The message type to use.
	Str:D $message-type = "m.notice",
) {
	$!client.send-event(
		$room-id,
		event-type => "m.room.message",
		content => {
			msgtype => $message-type,
			body => $body,
			format => "org.matrix.custom.html",
			formatted_body => parse-markdown($body).to-html,
		},
	);
}

#| Convenience method to distribute an event to all plugins. This method will
#| also ensure the handlers will run in their own threads, and that exceptions
#| are caught and handled slightly more gracefully.
method !distribute (
	#| The name of the method this event will be distributed to. This is merely
	#| for logging.
	Str:D $method,

	#| A code block which performs the actual distribution of an event to
	#| event-handlers.
	&code,
) {
	for @!plugins -> $plugin {
		my $trait = "Matrix::Bot/plugin({$plugin.^name})";

		start {
			CATCH {
				default { $trait.&get-logger.error($_) }
			}

			$trait.&get-logger.debug("Distributing event to {$plugin.^name}.$method");

			&code($plugin);
		}
	}
}

=begin pod

=NAME    Matrix::Bot
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.0

=head1 Synopsis

Matrix::Bot.new(:$home-server, :$username, :$password).run;

=head1 Description

A non-blocking framework to write Matrix bots in.

=head1 Examples

=head2 Basic usage with a simple plugin

=begin input
use Matrix::Bot;
use Matrix::Bot::Plugin;

class Local::Matrix::Plugin is Matrix::Bot::Plugin {
	multi method handle-event($e) {
		my $time = DateTime.new($e.origin_server_ts / 1000);

		say "$time <{$e.sender}> [{$e.room_id}] {$e.content<body>}";
	}
}

Matrix::Bot.new(
	home-server => "https://matrix.org",
	username => "Larry",
	password => $config<password>,
	plugins = [Local::Matrix::Plugin.new]
).run;
=end input

This bot will react on all events, and write an entry for them to STDOUT. You
can make use of Perl 6's
L<multi-dispatch|https://docs.perl6.org/language/glossary#index-entry-Multi-Dispatch>
and L<where
clauses|https://docs.perl6.org/type/Signature#index-entry-where_clause> to
filter out to just the events you want your plugins to handle.

=head2 Basic usage with convenience methods

Using the code of the previous example, but with a different plugin, you can
easily make a bot that responds to certain commands in a room.

=begin input
class Local::Matrix::Plugin is Matrix::Bot::Plugin {
	multi method handle-room-text ($e where *.message.fc.starts-with(".hi")) {
		"Hello, {$e.user}!"
	}
}
=end input

The C<handle-room-text> method is given a
C<Matrix::Bot::EventWrapper::RoomTextMessage> object, which is a StateEvent
that has undergone some transformations. This makes it easier for end-users to
write bots with them. Additionally, the C<handle-room-text> method will also
reply to the room with the string it returns, if any.

The C<event-handler> method is provided to make periodic or background tasks.
The following example will send a C<pong> message after 60 seconds a C<!ping>
message came in.

=begin input
class DelayPongBot is Matrix::Bot::Plugin {
	has $!supplier = Supplier.new;

	multi method handle-room-text($e where *.message.fc.starts-with("!ping")) {
		# Delay 60 seconds the message
		Promise.in(60).then({
			$!supplier.emit(Matrix::Bot::Event(:room-id($e.room), :message("pong")));
		});
        }

	multi method event-handler(--> Supply) {
		$!supplier.Supply
	}
}
=end input

=head1 See also

=item C<Matrix::Client>

=end pod

# vim: ft=perl6 noet
