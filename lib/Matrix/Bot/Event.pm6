#! /usr/bin/env false

use v6.d;

use LogP6;

unit class Matrix::Bot::Event;

has $.room-id;
has $.message;

method Str(--> Str) {
	"Matrix::Bot::Event<{$.room-id}>: $.message"
}
