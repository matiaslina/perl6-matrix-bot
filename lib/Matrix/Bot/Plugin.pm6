#! /usr/bin/env false
use v6.d;

use LogP6;

unit role Matrix::Bot::Plugin;

multi method handle-connect ($) { }

#| Receives all events. No additional convenience is applied to the return
#| value, you will need to interact with the client object directly.
multi method handle-event ($) { }

#| Receives only text messages sent to a room. If the method returns a
#| non-empty Str, this will be sent to back to the room.
multi method handle-room-text ($ --> Str) { Str }

#| Receives only invites to rooms. If the method returns True, the room will be
#| joined.
multi method handle-invite ($ --> Bool) { Bool }

#| Returns a supply that will receive L<Matrix::Bot::Event>
multi method event-handler($ --> Supply) { Nil }

#| Get a logger object. This can be used by 3rd party plugins to access the
#| standard logging mechanism.
method log()
{
	$?CLASS.^name.&get-logger;
}

=begin pod

=NAME    Matrix::Bot::Plugin
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
