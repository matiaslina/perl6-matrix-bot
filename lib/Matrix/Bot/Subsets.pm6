#! /usr/bin/env false

use v6.d;

unit module Matrix::Bot::Subsets;

subset Matrix::RoomID of Str where * ~~ / "!" \w ** 18 ":" .+ /;
subset Matrix::RoomAlias of Str where * ~~ / "#" .+? ":" .+ /;
subset Matrix::UserID of Str where * ~~ / "@" .+? ":" .+ /;

=begin pod

=NAME    Matrix::Bot::Subsets
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
