#! /usr/bin/env false

use v6.d;

use Matrix::Bot::Subsets;

#| A wrapper to handle invites to the client.
unit class Matrix::Bot::EventWrapper::RoomInvite;

#| The room ID the client was invited to.
has Matrix::RoomID $.room is required;

#| The display name of the room.
has Str $.name is required = "";

#| A list of aliases for the room.
has Str @.aliases;

#| The user that invited the client.
has Str $.user is required;

#| The time at which the invite was sent.
has DateTime $.time = now;

#| The full event details.
has $.event is required;

#| A reference to the bot itself.
has $.bot is required;

=begin pod

=NAME    Matrix::Bot::EventWrapper::RoomInvite
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
