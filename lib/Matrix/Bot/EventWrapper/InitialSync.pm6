#! /usr/bin/env false

use v6.d;

use Matrix::Bot::Subsets;

unit class Matrix::Bot::EventWrapper::InitialSync;

#| The rooms the bot is in.
has Matrix::RoomID @.rooms is required;

#| The core Matrix::Bot object.
has $.bot is required;

=begin pod

=NAME    Matrix::Bot::EventWrapper::InitialSync
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
