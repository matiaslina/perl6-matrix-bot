#! /usr/bin/env false

use v6.d;

use Matrix::Bot::Subsets;

#| A wrapper to make handling text messages sent to a room easier for
#| end-users.
unit class Matrix::Bot::EventWrapper::RoomTextMessage;

#| The timestamp of the message, as given by the origin server.
has DateTime $.time is required;

#| The user ID that sent the message.
has Matrix::UserID $.user is required;

#| The room ID in which the message was sent
has Matrix::RoomID $.room is required;

#| The text body of the message that was sent.
has Str $.message is required;

#| The full event details.
has $.event is required;

#| The core Matrix::Bot object.
has $.bot is required;

=begin pod

=NAME    Matrix::Bot::EventWrapper::RoomTextMessage
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
