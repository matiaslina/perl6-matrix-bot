# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [UNRELEASED]

### Removed

- `Matrix::Bot` no longer tries to log all messages it has received. This is
  better handled by a plugin instead.

## [0.2.0] - 2019-07-16

### Added

- The `Matrix::Bot::Plugin` base class has been given a `handle-invite` method.
  This method receives room invitation events. If the method returns `True`,
  the room will be joined.

- The `Matrix::Bot::Plugin` base class has been given a `log` method. This
  method can be used to access a `LogP6` logger object with the correct
  `trait`. The `trait` is currently the full class name of the plugin, but this
  may change at a later point in time.

  Related links:

  - https://modules.perl6.org/dist/LogP6:cpan:ATROXAPER
  - https://github.com/atroxaper/p6-LogP6-Blog-Example#readme

- The `Matrix::Bot::Plugin` base class has been given a `handle-connect`
  method. This method gets called exactly once, after the first synchronization
  call with the server. This method being called indicates the connection has
  been set up correctly, and the bot can start responding to events that happen
  on Matrix.

- The `Matrix::Bot` object now has an additional method to send Markdown
  formatted text to a room, `send-markdown`. This convenience method allows one
  to simply return a Markdown formatted string, and the framework will handle
  the rendering.

  Related links:

  - https://github.com/retupmoca/p6-markdown

### Changed

- The `handle-room-text` method now sends `m.notice` type messages. The Matrix
  spec encourages this type to be used for responses from automated clients,
  and such clients are the main user of this module.

  Related links:

  - [`GitLab#1`](https://gitlab.com/tyil/perl6-matrix-bot/issues/1)
  - https://matrix.org/docs/spec/client_server/r0.5.0#m-notice

  Special thanks to:

  - [matiaslina](https://gitlab.com/matiaslina)

- `handle-room-text` no longer receives a `Matrix::Client` instance, but
  instead receives a `Matrix::Bot` instance, stored in `$e.bot`. The
  `Matrix::Client` is still available in the `Matrix::Bot` instance. This
  change is intended to provide more power to the event handling methods of a
  plugin.

- `handle-room-text` now uses `Text::Markdown` to support markup in text.

  Related links:

  - [`GitLab#1`](https://gitlab.com/tyil/perl6-matrix-bot/issues/1)
  - https://github.com/retupmoca/p6-markdown

  Special thanks to:

  - [matiaslina](https://gitlab.com/matiaslina)

## [0.1.0] - 2019-07-15

- Initial release
